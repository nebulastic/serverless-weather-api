import os
import requests
import logging

cors_headers = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': 'true'
}


def get(event, context):
    base_url = 'https://api.openweathermap.org/data/2.5/forecast'

    try:
        city = event['queryStringParameters']['city']
        country = event['queryStringParameters']['country']
    except Exception as e:
        logging.error(e)
        return 'Wrong params', 404

    params = {
        'appid': os.getenv('API_KEY'),
        'units': 'metric',
        'q': f'{city},{country}'
    }

    r = requests.get(base_url, params=params)

    return {
        'statusCode': 200,
        'body': r.text,
        'headers': cors_headers
    }
